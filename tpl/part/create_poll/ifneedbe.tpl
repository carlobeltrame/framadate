{* Password *}

<div class="form-group">
    <label for="ifneedbe_enabled" class="col-sm-4 control-label">
        {t('Step 1', 'Allow "under reserve" answers')}
    </label>

    <div class="col-sm-8">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="ifneedbe_enabled" {if $poll_ifneedbe_enabled}checked{/if}
                       id="ifneedbe_enabled">
                {t('Step 1', 'Voters can answer "under reserve" in addition to "yes" and "no"')}
            </label>
        </div>
    </div>
</div>
